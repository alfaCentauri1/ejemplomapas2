package com.alfaCentauri;

import com.google.gson.Gson;

import java.util.*;

public class Ejecutable {

    public static void main(String[] args){
        System.out.println("Pruebas de tipos de datos Map: ");
        Object find = null;
        Gson json = new Gson();
        json.toJson("\"00\": \"Este es un valor\"");
        System.out.println("En el json: " + json);
        String []strList;
        //Inicializando el arreglo List
        for ( int i=0; i < 10; i++ ) {

        }
        //
        Map map = new HashMap();
//        map.put("List", strList);
        double cantidadDisponibleTotal = 0.0, cantidadDisponible = 0.0;
        List lineas = validateArray( map.get("LIST") );
        Map detalles = new HashMap();
        LinkedHashMap agrup = new LinkedHashMap();

        /* Debug */
        System.out.println("Drools: Lógica de agrupamiento");
        System.out.println("lineas: " + lineas);

        for ( Object item : lineas ) {
            Map element = (Map) item;
            String sku = (String) element.get("01");
            String loteCaja = (String) element.get("18");
            String almacen = (String) element.get("almacen");
            String lote = (String) element.get("lote");
            Object strCantidadDisponible = element.get("04");
            cantidadDisponible = Double.valueOf(strCantidadDisponible.toString());
            String fechaVencimiento = (String) element.get("fechaVencimiento");
            /* Debug */
            System.out.println("sku: " + sku);
            System.out.println("loteCaja: " + loteCaja);
            System.out.println("almacen: " + almacen);
            System.out.println("lote: " + lote);
            System.out.println("cantidad: " + strCantidadDisponible);
            System.out.println("cantidadDisponible: " + cantidadDisponible);
            System.out.println("fechaVencimiento: " + fechaVencimiento);
            /* Crea el nombre para el jsonArray */
            StringBuilder keyName = new StringBuilder();
            keyName.append(sku);
            keyName.append("_");
            keyName.append(loteCaja);
            keyName.append("_");
            keyName.append(almacen);
            keyName.append("_");
            keyName.append(lote);
            System.out.println(keyName.toString());
            /* Busca el keyName en el listado de mapas de detalles */
            find = agrup.get(keyName.toString());
            /* Existe el SKU. Guardar en un nuevo arreglo de detalles */
            if (find != null) {
                /* Recupera el elemento del arreglo de detalles */
                Map itemFound = (Map) find;
                /* Debug */
                System.out.println("Existe: " + sku);
                System.out.println("Encontré: " + itemFound);
                /* Suma las cantidades disponibles */
                Object strQuantity = itemFound.get("cantidadDisponibleTotal");
                double availableQuantityOfItemFound = Double.valueOf( strQuantity.toString() );
                cantidadDisponibleTotal = cantidadDisponible + availableQuantityOfItemFound;
                itemFound.put("cantidadDisponibleTotal", cantidadDisponibleTotal);
                /* Debug */
                System.out.println("cantidadDisponible " + cantidadDisponible);
                System.out.println("availableQuantityOfItemFound " + availableQuantityOfItemFound);
                System.out.println("cantidadDisponibleTotal " + cantidadDisponibleTotal);
            }
            else {
                /* No existe y se crea uno nuevo */
                Map elementDetail = new HashMap();
                elementDetail.put("sku", sku);
                elementDetail.put("loteCaja", loteCaja);
                elementDetail.put("almacen", almacen);
                elementDetail.put("lote", lote);
                elementDetail.put("cantidadDisponibleTotal", cantidadDisponibleTotal);
                elementDetail.put("fechaVencimiento", fechaVencimiento);
                detalles.put(keyName, elementDetail);
                /* Debug */
                System.out.println("Nuevo Elemento " + elementDetail);
                System.out.println("Nuevo Elemento " + elementDetail.values());
                agrup.put(keyName.toString(),elementDetail);
            }
        }
        //
        System.out.println("agrup: " + agrup);
        /* Debug */
        System.out.println("Resultado del map: " + map);
        System.out.println("Detalles: " + detalles);
        /**/
        map.put("detalles", detalles);
    }

    /**
     * @param jsonInput  Type Object.
     * @return Return a arraylist with data or arraylist empty.
     **/
    static List validateArray(Object jsonInput) {
        List detalles = new ArrayList();
        if ( jsonInput != null ) {
            if (jsonInput instanceof ArrayList) {
                detalles = (ArrayList)jsonInput;
            } else {
                detalles.add((Map)jsonInput);
            }
        }
        return detalles;
    }
}
