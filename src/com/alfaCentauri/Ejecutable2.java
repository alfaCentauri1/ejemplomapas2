package com.alfaCentauri;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Ejecutable2 {

    public static void main(String[] args) {
        System.out.println("Pruebas de tipos de datos LinkedHashMap: ");
        String propertys = "{\n" +
                "    \"type\": \"bisa_846\",\n" +
                "    \"from\": \"bisa\",\n" +
                "    \"jsonTemplate\": {\n" +
                "        \"YMM_3PL_IF_SNAPSHOT01\": {\n" +
                "            \"IDOC\": {\n" +
                "                \"-BEGIN\": \"\",\n" +
                "                \"EDI_DC40\": {\n" +
                "                    \"-SEGMENT\": \"\",\n" +
                "                    \"TABNAM\": \"\",\n" +
                "                    \"MANDT\": \"\",\n" +
                "                    \"DOCNUM\": \"\",\n" +
                "                    \"DOCREL\": \"\",\n" +
                "                    \"STATUS\": \"\",\n" +
                "                    \"DIRECT\": \"\",\n" +
                "                    \"OUTMOD\": \"\",\n" +
                "                    \"IDOCTYP\": \"\",\n" +
                "                    \"MESTYP\": \"\",\n" +
                "                    \"SNDPOR\": \"\",\n" +
                "                    \"SNDPRT\": \"\",\n" +
                "                    \"SNDPRN\": \"\",\n" +
                "                    \"RCVPOR\": \"\",\n" +
                "                    \"RCVPRT\": \"\",\n" +
                "                    \"RCVPRN\": \"\",\n" +
                "                    \"CREDAT\": \"\",\n" +
                "                    \"CRETIM\": \"\",\n" +
                "                    \"REFMES\": \"\",\n" +
                "                    \"ARCKEY\": \"\"\n" +
                "                },\n" +
                "                \"Y1MM_3PL_IF_SNAPSHOT_HEADER\": {\n" +
                "                    \"-SEGMENT\": \"\",\n" +
                "                    \"SNSDAT\": \"\",\n" +
                "                    \"SNSTIM\": \"\",\n" +
                "                    \"RKHEAD\": \"\",\n" +
                "                    \"Y1MM_3PL_IF_SNAPSHOT_LINE\": [\n" +
                "                        {\n" +
                "                            \"-SEGMENT\": \"\",\n" +
                "                            \"MATNR\": \"\",\n" +
                "                            \"MATNR_EXTERNO\": \"\",\n" +
                "                            \"CHARG\": \"\",\n" +
                "                            \"STOTYP\": \"\",\n" +
                "                            \"WERKS\": \"\",\n" +
                "                            \"LGORT\": \"\",\n" +
                "                            \"ERFMG\": \"\",\n" +
                "                            \"ERFME\": \"\"\n" +
                "                        }\n" +
                "                    ]\n" +
                "                }\n" +
                "            }\n" +
                "        },\n" +
                "        \"openConfig\": [\n" +
                "            {\n" +
                "                \"type\": \"Object\",\n" +
                "                \"inputPosition\": \"\",\n" +
                "                \"outputPosition\": \"YMM_3PL_IF_SNAPSHOT01\",\n" +
                "                \"fields\": [\n" +
                "                    {\n" +
                "                        \"type\": \"Object\",\n" +
                "                        \"inputPosition\": \"\",\n" +
                "                        \"outputPosition\": \"IDOC\",\n" +
                "                        \"fields\": [\n" +
                "                            {\n" +
                "                                \"type\": \"java.util.String\",\n" +
                "                                \"inputPosition\": \"\",\n" +
                "                                \"outputPosition\": \"-BEGIN\",\n" +
                "                                \"maskin\": \"\",\n" +
                "                                \"maskout\": \"\",\n" +
                "                                \"prefixedValue\": \"1\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"type\": \"Object\",\n" +
                "                                \"inputPosition\": \"\",\n" +
                "                                \"outputPosition\": \"EDI_DC40\",\n" +
                "                                \"fields\": [\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"-SEGMENT\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"1\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"TABNAM\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"EDI_DC40\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"MANDT\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"005\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"DOCNUM\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"3147\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"DOCREL\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"731\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"STATUS\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"53\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"DIRECT\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"2\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"OUTMOD\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \" \"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"IDOCTYP\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"YMM_3PL_IF_SNAPSHOT01\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"MESTYP\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"YMM_3PL_IF_SNAPSHOT\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"SNDPOR\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"SAPPXP\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"SNDPRT\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"LS\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"SNDPRN\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"3PLARA_AND\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"RCVPOR\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"SAPGEU\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"RCVPRT\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"LS\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"RCVPRN\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"GEPCLNT005\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"CREDAT\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \" \"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"CRETIM\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \" \"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"REFMES\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"3147\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"ARCKEY\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"EE A5B093C663B011E5BD50000010E02A4F 3064327773509829\"\n" +
                "                                    }\n" +
                "                                ]\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"type\": \"Object\",\n" +
                "                                \"inputPosition\": \"\",\n" +
                "                                \"outputPosition\": \"Y1MM_3PL_IF_SNAPSHOT_HEADER\",\n" +
                "                                \"fields\": [\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"-SEGMENT\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \"1\"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"SNSDAT\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \" \"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"SNSTIM\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \" \"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.String\",\n" +
                "                                        \"inputPosition\": \"\",\n" +
                "                                        \"outputPosition\": \"RKHEAD\",\n" +
                "                                        \"maskin\": \"\",\n" +
                "                                        \"maskout\": \"\",\n" +
                "                                        \"prefixedValue\": \" \"\n" +
                "                                    },\n" +
                "                                    {\n" +
                "                                        \"type\": \"java.util.ArrayList\",\n" +
                "                                        \"inputPosition\": \"LIST\",\n" +
                "                                        \"outputPosition\": \"Y1MM_3PL_IF_SNAPSHOT_LINE\",\n" +
                "                                        \"fields\": [\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"\",\n" +
                "                                                \"outputPosition\": \"-SEGMENT\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \"1\"\n" +
                "                                            },\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"LIST,01\",\n" +
                "                                                \"outputPosition\": \"MATNR\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \" \"\n" +
                "                                            },\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"LIST,08\",\n" +
                "                                                \"outputPosition\": \"MATNR_EXTERNO\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \" \"\n" +
                "                                            },\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"LIST,18\",\n" +
                "                                                \"outputPosition\": \"CHARG\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \" \"\n" +
                "                                            },\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"\",\n" +
                "                                                \"outputPosition\": \"STOTYP\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \" \"\n" +
                "                                            },\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"\",\n" +
                "                                                \"outputPosition\": \"WERKS\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \"AR20\"\n" +
                "                                            },\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"\",\n" +
                "                                                \"outputPosition\": \"LGORT\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \" \"\n" +
                "                                            },\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"LIST,04\",\n" +
                "                                                \"outputPosition\": \"ERFMG\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \" \"\n" +
                "                                            },\n" +
                "                                            {\n" +
                "                                                \"type\": \"java.util.String\",\n" +
                "                                                \"inputPosition\": \"\",\n" +
                "                                                \"outputPosition\": \"ERFME\",\n" +
                "                                                \"maskin\": \"\",\n" +
                "                                                \"maskout\": \"\",\n" +
                "                                                \"prefixedValue\": \"EA\"\n" +
                "                                            }\n" +
                "                                        ]\n" +
                "                                    }\n" +
                "                                ]\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    }\n" +
                "                ]\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";
        Map mapa = new HashMap<>();
        Gson json = new Gson();
        json.fromJson("{ \"titulo\": \"El titulo es el #1\", \"numero\": 5, \"unidad\": \"px\"}", "String".getClass());
        System.out.println("Resultado de json: " + json);
        mapa.put("linea1", "123");
        mapa.put("linea2", "abc");
        mapa.put("linea3", "567");
        mapa.put("linea4", "fgh");
        LinkedHashMap listado = new LinkedHashMap<>(mapa);
        System.out.println("Resultado: " + listado.toString());
    }
}
