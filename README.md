# EjemploMapas2
Este es un ejemplo del uso de Map en Java, con valores string y decimales.


## Contenido


## Requisitos

* JDK 18 ó superior.
* Librerias de Maven.

## Instalación

* Compile el proyecto con el IDE de su preferencia.
* Ejecute desde la consola; en la raíz del proyecto, java out/production/ejemplomapas2/com/alfaCentauri/ejecutable.jar

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
* Developer